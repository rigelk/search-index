
import { Account, AccountSummary, Avatar, Video, VideoChannel, VideoChannelSummary, VideoDetails } from '../../PeerTube/shared/models'
import { IndexableDoc } from './elastic-search.model'

type ActorExtended = {
  handle: string
  avatar: Avatar & { url: string }
}

export interface IndexableVideo extends Video, IndexableDoc {
}

export interface IndexableVideoDetails extends VideoDetails, IndexableDoc {
}

export interface DBVideoDetails extends Omit<VideoDetails, 'isLocal'> {
  indexedAt: Date
  host: string

  account: Account & ActorExtended
  channel: VideoChannel & ActorExtended

  score?: number
}

export interface DBVideo extends Omit<Video, 'isLocal'> {
  indexedAt: Date
  host: string
  url: string

  account: AccountSummary & ActorExtended
  channel: VideoChannelSummary & ActorExtended
}

// Results from the search API
export interface EnhancedVideo extends Video {
  tags: VideoDetails['tags']

  score: number
}
