import * as express from 'express'
import { VideosIndexer } from '../../lib/schedulers/videos-indexer'
import { ServerConfig } from '../../../shared'
import { CONFIG } from '../../initializers/constants'

const configRouter = express.Router()

configRouter.get('/config',
  getConfig
)

// ---------------------------------------------------------------------------

export { configRouter }

// ---------------------------------------------------------------------------

async function getConfig (req: express.Request, res: express.Response) {
  return res.json({
    searchInstanceName: CONFIG.SEARCH_INSTANCE.NAME,
    legalNoticesUrl: CONFIG.SEARCH_INSTANCE.LEGAL_NOTICES_URL,
    indexedHostsCount: VideosIndexer.Instance.getIndexedHosts().length,
    indexedInstancesUrl: CONFIG.INSTANCES_INDEX.PUBLIC_URL
  } as ServerConfig)
}
