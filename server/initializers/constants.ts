import * as config from 'config'
import { isTestInstance } from '../helpers/core-utils'

const API_VERSION = 'v1'

const CONFIG = {
  LISTEN: {
    PORT: config.get<number>('listen.port')
  },
  WEBSERVER: {
    SCHEME: config.get<boolean>('webserver.https') === true ? 'https' : 'http',
    HOSTNAME: config.get<string>('webserver.hostname'),
    PORT: config.get<number>('webserver.port')
  },
  ELASTIC_SEARCH: {
    HOSTNAME: config.get<string>('elastic_search.hostname'),
    PORT: config.get<number>('elastic_search.port'),
    INDEXES: {
      VIDEOS: config.get<string>('elastic_search.indexes.videos'),
      CHANNELS: config.get<string>('elastic_search.indexes.channels')
    }
  },
  LOG: {
    LEVEL: config.get<string>('log.level')
  },
  SEARCH_INSTANCE: {
    NAME: config.get<string>('search-instance.name'),
    DESCRIPTION: config.get<string>('search-instance.description'),
    LEGAL_NOTICES_URL: config.get<string>('search-instance.legal_notices_url')
  },
  INSTANCES_INDEX: {
    URL: config.get<string>('instances-index.url'),
    PUBLIC_URL: config.get<string>('instances-index.public_url'),
    WHITELIST: {
      ENABLED: config.get<boolean>('instances-index.whitelist.enabled'),
      HOSTS: config.get<string[]>('instances-index.whitelist.hosts')
    }
  },
  API: {
    BLACKLIST: {
      ENABLED: config.get<boolean>('api.blacklist.enabled'),
      HOSTS: config.get<string[]>('api.blacklist.hosts')
    }
  }
}

const SORTABLE_COLUMNS = {
  VIDEOS_SEARCH: [ 'name', 'duration', 'createdAt', 'publishedAt', 'originallyPublishedAt', 'views', 'likes', 'match' ],
  CHANNELS_SEARCH: [ 'match', 'displayName', 'createdAt' ]
}

const PAGINATION_COUNT_DEFAULT = 20

const SCHEDULER_INTERVALS_MS = {
  videosIndexer: 60000 * 60 * 24 // 24 hours
}

const INDEXER_COUNT = {
  VIDEOS: 10
}

const INDEXER_CONCURRENCY = 3

const INDEXER_QUEUE_CONCURRENCY = 3

const REQUESTS = {
  MAX_RETRIES: 10,
  WAIT: 10000 // 10 seconds
}

function getWebserverUrl () {
  if (CONFIG.WEBSERVER.PORT === 80 || CONFIG.WEBSERVER.PORT === 443) {
    return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME
  }

  return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME + ':' + CONFIG.WEBSERVER.PORT
}

if (isTestInstance()) {
  SCHEDULER_INTERVALS_MS.videosIndexer = 1000 * 60 * 5 // 5 minutes
}

export {
  getWebserverUrl,

  CONFIG,
  API_VERSION,
  PAGINATION_COUNT_DEFAULT,
  SORTABLE_COLUMNS,
  INDEXER_QUEUE_CONCURRENCY,
  SCHEDULER_INTERVALS_MS,
  INDEXER_CONCURRENCY,
  INDEXER_COUNT,
  REQUESTS
}
