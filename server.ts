import { isTestInstance } from './server/helpers/core-utils'

if (isTestInstance()) {
  require('source-map-support').install()
}

import * as bodyParser from 'body-parser'
import * as express from 'express'
import * as cors from 'cors'
import * as morgan from 'morgan'
import { apiRouter } from './server/controllers/api'
import { logger } from './server/helpers/logger'
import { API_VERSION, CONFIG, getWebserverUrl } from './server/initializers/constants'
import { VideosIndexer } from './server/lib/schedulers/videos-indexer'
import { initVideosIndex } from './server/lib/elastic-search-videos'
import { initChannelsIndex } from './server/lib/elastic-search-channels'
import { join } from 'path'
import { send } from 'process'
import { readFile } from 'fs-extra'

const app = express()

app.use(morgan('combined', {
  stream: { write: logger.info.bind(logger) }
}))

app.use(bodyParser.json({
  type: [ 'application/json', 'application/*+json' ],
  limit: '5mb'
}))
app.use(bodyParser.urlencoded({ extended: false }))

// ----------- Views, routes and static files -----------

app.use(cors())

const apiRoute = '/api/' + API_VERSION
app.use(apiRoute, apiRouter)

// Static client files
app.use('/js/', express.static(join(__dirname, '../client/dist/js')))
app.use('/css/', express.static(join(__dirname, '../client/dist/css')))
app.use('/img/', express.static(join(__dirname, '../client/dist/img')))

let indexHTML: string

app.use('/*', async function (req, res) {
  res.set('Content-Type', 'text/html; charset=UTF-8')

  if (indexHTML) return res.send(indexHTML)

  const buffer = await readFile(join(__dirname, '../client/dist/index.html'))

  const url = getWebserverUrl()
  const title = CONFIG.SEARCH_INSTANCE.NAME
  const description = CONFIG.SEARCH_INSTANCE.DESCRIPTION

  const tags = `
  <meta name="description" content="${description}">

  <meta property="og:url" content="${url}">
  <meta property="og:title" content="${title}">
  <meta property="og:description" content="${description}">
  <meta property="og:image" content="${url}/img/card-opengraph.png">
  <meta property="og:site_name" content="${title}">

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@joinpeertube">
  <meta name="twitter:creator" content="@chocobozzz">
  <meta name="twitter:title" content="${title}">
  <meta name="twitter:description" content="${description}">
  <meta name="twitter:image" content="${url}/img/card-opengraph.png">`

  indexHTML = buffer.toString()
  indexHTML = indexHTML.replace('</head>', tags + '</head>')

  return res.send(indexHTML)
})

// ----------- Errors -----------

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found') as any
  err.status = 404
  next(err)
})

app.use(function (err, req, res, next) {
  let error = 'Unknown error.'
  if (err) {
    error = err.stack || err.message || err
  }

  logger.error({ error }, 'Error in controller.')
  return res.status(err.status || 500).end()
})

// ----------- Run -----------

app.listen(CONFIG.LISTEN.PORT, async () => {
  logger.info('Server listening on port %d', CONFIG.LISTEN.PORT)

  try {
    await Promise.all([
      initVideosIndex(),
      initChannelsIndex()
    ])
  } catch (err) {
    logger.error('Cannot init videos index.', { err })
    process.exit(-1)
  }

  VideosIndexer.Instance.enable()
  VideosIndexer.Instance.execute()
    .catch(err => logger.error('Cannot run video indexer', { err }))
})
