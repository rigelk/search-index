export interface ServerConfig {
  searchInstanceName: string

  indexedHostsCount: number

  indexedInstancesUrl: string

  legalNoticesUrl: string
}
